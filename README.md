> ⚠ Please note:  
> This README was retroactively created from the
> [original forum post](http://forum.sa-mp.com/showthread.php?t=226953)
> and was modified slightly to remove stuff like weird typos and expired
> links.

# Extinguishable Fire!
> v0.4b

Hi 😀  
Today I present to you my "ExtFire"-Filterscript!

## Intro
Yea, what can I tell you here? 😀  
I think everyone always wanted to control the fire in San Andras.  
"ExtFire" gives you the ability to lay fire where you want, extinguish
it and let players burn, too!

## Functions
<u>With this Filterscript you can do this:</u>
*	With /fire you can create a fire, if enabled in Pawn.
*	Fire can be extinguished on 3 ways.
*	If you walk through a flame WITHOUT being a fireman (skin), you'll
	get set on fire.
*	If you touch some burning people, you also start to burn 😮

You can extinguish flames with
*	the extinguisher (weapon ID `42`),
*	a firetruck (model `407`),
*	a SWAT Van (Model 601)
*	and with the special action `SPECIAL_ACTION_PISSING` 😎

If "Flame ahead" is displayed on your screen, the extinguishing process
just begun.

**<u>Required</u>: SA-MP 0.3c RC1** or higher.

**<u>Note:</u>**
*	This fire acts like normal San Andreas Fire, it damages the player
	and burns as long as normal fire!
*	Players earn money, if they extinguish a fire. You can simply
	disable this with commenting this out:
	```c
	#define EarnMoney
	```
*	This Filterscript is multi-langual: German and english. Just change
	line 10.
	```c
	#define German // German or English?
	```

### Functions & Callbacks
```c
CreateFire(Float:x, Float:y, Float:z);
CreateSmoke(Float:x, Float:y, Float:z);
DestroyFire(id);
DestroySmoke(id);
RemoveSmokeFromFire(id);
TogglePlayerBurning(playerid, burning);
GetFireID(Float:x, Float:y, Float:z, &Float:dist);

OnFireUpdate();
ExtinguishTimer(playerid, id);
BurningTimer(playerid);StopPlayerBurning(playerid);
```

To config when a player can burn, you can add this to your gamemode
easily:
```c
public CanBurn(playerid)
{
	//check ...
	return 1; //0 = false, 1 = true
}
```

## Screenshots
![Screenshot 1](docs/img/f1.png)  
![Screenshot 2](docs/img/f2.png)  
![Screenshot 3](docs/img/f3.png)

## End note
Have fun!
