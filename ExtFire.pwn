#include <a_samp>
#pragma tabsize 0

#define VERSION "0.4b"

#define MAX_FIRES					100			// max amout of fires able to exist at once
#define BurnOthers								// should players be able to ignite other players by touching?
#define FireMessageColor			0x00FF55FF	// color used for the extinguishing message

#define BURNING_RADIUS 				1.2     	// radius in which you start burning if you're too close to a fire
#define ONFOOT_RADIUS				1.5			// radius in which you can extinguish the fire by foot
#define PISSING_DISTANCE			2.0			// radius in which you can extinguish the fire by peeing
#define CAR_RADIUS					8.0			// radius in which you can extinguish the fire by firetruck/SWAT Van
#define Z_DIFFERENCE				2.5			// height which is used for technical accurancy of extinguishing. Please do not change
#define FIRE_UPDATE_TIMER_DELAY     500     	// amount of milliseconds the fire timer should loop in
#define EXTINGUISH_TIME_VEHICLE		1			// time you have to spray at the fire with a firetruck (seconds)
#define EXTINGUISH_TIME_ONFOOT		4			// time you have to spray at the fire onfoot (seconds)
#define EXTINGUISH_TIME_PEEING		10			// time you have to pee at the fire (seconds)
#define EXTINGUISH_TIME_PLAYER		2			// time it takes to extinguish a player (seconds)
#define FIRE_OBJECT_SLOT			1			// the slot used with SetPlayerAttachedObject and RemovePlayerAttachedObject

//#define German								// German or English?
//#define MessageToAll							// Should the extinguishing message be sent to all players?

#if !defined SPECIAL_ACTION_PISSING
	#define SPECIAL_ACTION_PISSING	(68)
#endif

//             Using a streamer? Maybe that would come handy ...
//#define DRAW_DISTANCE 200.0
//#define CreateObject(%0) CreateDynamicObject(%0, -1, -1, -1, DRAW_DISTANCE)
//#define DestroyObject(%0) DestroyDynamicObject(%0)

//===================== forwards ====================

forward CreateFire(Float:x, Float:y, Float:z);
forward CreateSmoke(Float:x, Float:y, Float:z);
forward DestroyFire(id);
forward DestroySmoke(id);
forward RemoveSmokeFromFire(id);
forward TogglePlayerBurning(playerid, burning);

forward OnFireUpdate();
forward ExtinguishTimer(playerid, id);
forward BurningTimer(playerid);

//===================== Variables ====================

enum FlameInfo
{
	Flame_id,
	Flame_Exists,
	Float:Flame_pos[3],
	Smoke[5],
}

new Flame[MAX_FIRES][FlameInfo];
new PlayerFireTimer[MAX_PLAYERS][3]; // Burn, StopBurn, Extinguish a fire
new Float:PlayerOnFireHP[MAX_PLAYERS];

//===================== Normal Publics ====================

public OnGameModeInit() { OnFilterScriptInit(); }
public OnGameModeExit() { OnFilterScriptExit(); }

public OnFilterScriptInit()
{
    AntiDeAMX();

	for(new i; i < MAX_PLAYERS; i++)
	{
	    PlayerFireTimer[i][2] = -1;
	}

    print(" ");
	print("      /\\_/¯\\_/¯\\_/¯\\_/¯\\_/¯\\_/\\");
	print("     <                         >");
 	#if defined German
	print("     >   Löschbares Feuer "#VERSION" <");
	#else
	print("     > Extinguishable Fire "#VERSION" <");
	#endif
 	#if defined German
	print("     <        von Meta         >");
	#else
	print("     <         by Meta         >");
	#endif
	print("      \\/¯\\_/¯\\_/¯\\_/¯\\_/¯\\_/¯\\/\n");
	SetTimer("OnFireUpdate", FIRE_UPDATE_TIMER_DELAY, 1);
	return 1;
}

public OnFilterScriptExit()
{
	for(new i; i < MAX_FIRES; i++)
	{
	    DestroyFire(i);
	}
	for(new playerid; playerid < MAX_PLAYERS; playerid++)
	{
		if(GetPVarInt(playerid, "IsOnFire") && !CanPlayerBurn(playerid, 1))
		{
			TogglePlayerBurning(playerid, false);
		}
	}
	return 1;
}

public OnPlayerCommandText(playerid, cmdtext[])
{
	new idx, cmd[256];
	cmd = strtok(cmdtext, idx);
	if(IsPlayerAdmin(playerid))
	{
		#if defined German
		if(strcmp("/feuer", cmd, true) == 0)
		#else
		if(strcmp("/fire", cmd, true) == 0)
		#endif
		{
      		new Float:x, Float:y, Float:z, Float:a;
		    GetXYInFrontOfPlayer(playerid, x, y, z, a, 2.5);
		    /*CallRemoteFuntion("*/CreateFire(/*", "fff", */x, y, z);
			return 1;
		}
	}
	return 0;
}

public OnPlayerDeath(playerid, killerid, reason)
{
    if(GetPVarInt(playerid, "IsOnFire"))
	{
	    #if defined German
		SendClientMessage(playerid, 0xff000000, "Du bist verbrannt!");
		#else
		SendClientMessage(playerid, 0xff000000, "You got fried!");
		#endif
		TogglePlayerBurning(playerid, false);
	}
}

public OnFireUpdate()
{
	new aim, piss;
	for(new playerid; playerid < MAX_PLAYERS; playerid++)
	{
        aim = -1; piss = -1;
	    if(!IsPlayerConnected(playerid) || IsPlayerNPC(playerid)) { continue; }
		if(GetPVarInt(playerid, "IsOnFire") && !CanPlayerBurn(playerid, 1))
		{
			TogglePlayerBurning(playerid, false);
		}
		if(Pissing_at_Flame(playerid) != -1 || Aiming_at_Flame(playerid) != -1)
		{
			piss = Pissing_at_Flame(playerid); aim = Aiming_at_Flame(playerid);

		    #if defined German
			GameTextForPlayer(playerid, " ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~r~~h~Feuer in Sicht", 1500, 6);
			#else
			GameTextForPlayer(playerid, " ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~n~ ~r~~h~Fire in sight", 1500, 6);
			#endif
			if(PlayerFireTimer[playerid][2] == -1 && ((aim != -1 && Pressing(playerid) & KEY_FIRE) || piss != -1))
			{
			    new value, time, Float:x, Float:y, Float:z;
			    if(piss != -1)
			    {
					value = piss;
					time = EXTINGUISH_TIME_PEEING;
				}
				else if(aim != -1)
				{
					value = aim;
					if(GetPlayerWeapon(playerid) == 41)
					{
						CreateExplosion(Flame[value][Flame_pos][0], Flame[value][Flame_pos][1], Flame[value][Flame_pos][2], 2, 5);
						continue;
					}
					if(IsPlayerInAnyVehicle(playerid))
					{
					    time = EXTINGUISH_TIME_VEHICLE;
					}
					else
					{
						time = EXTINGUISH_TIME_ONFOOT;
					}
				}
				if(value < -1) { time = EXTINGUISH_TIME_PLAYER; }
				time *= 1000;
				if(value >= -1)
				{
					x = Flame[value][Flame_pos][0];
				    y = Flame[value][Flame_pos][1];
				    z = Flame[value][Flame_pos][2];
				    RemoveSmokeFromFire(value);
					Flame[value][Smoke][0] = CreateObject(18727, x, y, z, 0.0, 0.0, 0.0);
					Flame[value][Smoke][1] = CreateObject(18727, x+1, y, z, 0.0, 0.0, 0.0);
					Flame[value][Smoke][2] = CreateObject(18727, x-1, y, z, 0.0, 0.0, 0.0);
					Flame[value][Smoke][3] = CreateObject(18727, x, y+1, z, 0.0, 0.0, 0.0);
					Flame[value][Smoke][4] = CreateObject(18727, x, y-1, z, 0.0, 0.0, 0.0);
				}
				PlayerFireTimer[playerid][2] = SetTimerEx("ExtinguishTimer", time, 0, "dd", playerid, value);
			}
		}
		if(CanPlayerBurn(playerid) && IsAtFlame(playerid))
		{
			TogglePlayerBurning(playerid, true);
		}
		#if defined BurnOthers
		new Float:x, Float:y, Float:z;
		for(new i; i < MAX_PLAYERS; i++)
	  	{
	  	    if(playerid != i && IsPlayerConnected(i) && !IsPlayerNPC(i))
		  	{
			  	if(CanPlayerBurn(i) && GetPVarInt(playerid, "IsOnFire") && !GetPVarInt(i, "IsOnFire"))
	  	    	{
				  	GetPlayerPos(i, x, y, z);
					if(IsPlayerInRangeOfPoint(playerid, BURNING_RADIUS, x, y, z))
					{
					    TogglePlayerBurning(i, true);
					}
				}
			}
		}
		#endif
 	}
	return 1;
}


//===================== stocks ====================

stock GetXYInFrontOfPlayer(playerid, &Float:x, &Float:y, &Float:z, &Float:a, Float:distance)
{
	GetPlayerPos(playerid, x, y ,z);
	if(IsPlayerInAnyVehicle(playerid))
	{
		GetVehicleZAngle(GetPlayerVehicleID(playerid),a);
	}
	else
	{
		GetPlayerFacingAngle(playerid, a);
	}
	x += (distance * floatsin(-a, degrees));
	y += (distance * floatcos(-a, degrees));
	return 0;
}

stock strtok(const string[], &index)
{
    new length = strlen(string);
    while ((index < length) && (string[index] <= ' '))
    {
        index++;
    }
    new offset = index;
    new result[256];
    while ((index < length) && (string[index] > ' ') && ((index - offset) < (sizeof(result) - 1)))
    {
        result[index - offset] = string[index];
        index++;
    }
    result[index - offset] = EOS;
    return result;
}

#if !defined ReturnUser
stock ReturnUser(text[])
{
	new pos = 0;
	while (text[pos] < 0x21)
	{
		if(text[pos] == 0) return INVALID_PLAYER_ID;
		pos++;
	}
	new userid = INVALID_PLAYER_ID;
	if(isNumeric(text[pos]))
	{
		userid = strval(text[pos]);
		if(userid >=0 && userid < MAX_PLAYERS)
		{
			if(!IsPlayerConnected(userid))
				userid = INVALID_PLAYER_ID;
			else return userid;
		}
	}
	new len = strlen(text[pos]);
	new count = 0;
	new pname[MAX_PLAYER_NAME];
	for (new i = 0; i < MAX_PLAYERS; i++)
	{
		if(IsPlayerConnected(i))
  		{
			GetPlayerName(i, pname, sizeof (pname));
			if(strcmp(pname, text[pos], true, len) == 0)
			{
				if(len == strlen(pname)) return i;
				else
				{
					count++;
					userid = i;
				}
			}
		}
	}
	if(count != 1)
	{
		userid = INVALID_PLAYER_ID;
	}
	return userid;
}
#endif
#if !defined isNumeric
stock isNumeric(const string[])
{
	new length=strlen(string);
	if (length==0) return false;
	for (new i = 0; i < length; i++)
	{
		if ((string[i] > '9' || string[i] < '0' && string[i]!='-' && string[i]!='+') /*Not a number,'+' or '-'*/|| (string[i]=='-' && i!=0)/* A '-' but not at first.*/|| (string[i]=='+' && i!=0)/* A '+' but not at first.*/)
		{
			return false;
		}
	}
	if (length==1 && (string[0]=='-' || string[0]=='+')) { return false; }
	return true;
}
#endif

stock Float:GetDistanceBetweenPoints(Float:x1,Float:y1,Float:z1,Float:x2,Float:y2,Float:z2) //By Gabriel "Larcius" Cordes
{
	return floatadd(floatadd(floatsqroot(floatpower(floatsub(x1,x2),2)),floatsqroot(floatpower(floatsub(y1,y2),2))),floatsqroot(floatpower(floatsub(z1,z2),2)));
}

stock Float:DistanceCameraTargetToLocation(Float:CamX, Float:CamY, Float:CamZ, Float:ObjX, Float:ObjY, Float:ObjZ, Float:FrX, Float:FrY, Float:FrZ)
{
	new Float:TGTDistance;

	// get distance from camera to target
	TGTDistance = floatsqroot((CamX - ObjX) * (CamX - ObjX) + (CamY - ObjY) * (CamY - ObjY) + (CamZ - ObjZ) * (CamZ - ObjZ));

	new Float:tmpX, Float:tmpY, Float:tmpZ;

	tmpX = FrX * TGTDistance + CamX;
	tmpY = FrY * TGTDistance + CamY;
	tmpZ = FrZ * TGTDistance + CamZ;

	return floatsqroot((tmpX - ObjX) * (tmpX - ObjX) + (tmpY - ObjY) * (tmpY - ObjY) + (tmpZ - ObjZ) * (tmpZ - ObjZ));
}

stock IsPlayerAimingAt(playerid, Float:x, Float:y, Float:z, Float:radius)
{
	new Float:cx,Float:cy,Float:cz,Float:fx,Float:fy,Float:fz;
	GetPlayerCameraPos(playerid, cx, cy, cz);
	GetPlayerCameraFrontVector(playerid, fx, fy, fz);
	return (radius >= DistanceCameraTargetToLocation(cx, cy, cz, x, y, z, fx, fy, fz));
}

//===================== Own Publics ====================

public CreateFire(Float:x, Float:y, Float:z)
{
	new slot = GetFlameSlot();
	if(slot == -1) {return slot;}
	Flame[slot][Flame_Exists] = 1;
	Flame[slot][Flame_pos][0] = x;
	Flame[slot][Flame_pos][1] = y;
	Flame[slot][Flame_pos][2] = z - Z_DIFFERENCE;
	Flame[slot][Flame_id] = CreateObject(18689, Flame[slot][Flame_pos][0], Flame[slot][Flame_pos][1], Flame[slot][Flame_pos][2], 0.0, 0.0, 0.0);

	for(new i; i < 5; i++)
	{
		Flame[slot][Smoke][i] = -1;
	}
	return slot;
}

public DestroyFire(id)
{
 	DestroyObject(Flame[id][Flame_id]);
	Flame[id][Flame_Exists] = 0;
	Flame[id][Flame_pos][0] = 0.0;
	Flame[id][Flame_pos][1] = 0.0;
	Flame[id][Flame_pos][2] = 0.0;
	RemoveSmokeFromFire(id);
}

//# A suggestion from a user of this script. Very simple functions to add and remove smoke without flames.
//# Think about a way to kill the smoke and use it, if you wish.
//# Maybe you could link smoke on a house with variables to a flame inside a house so if the flame gets extinguished the smoke disappears.

public CreateSmoke(Float:x, Float:y, Float:z)
{
	return CreateObject(18727, x, y, z, 0.0, 0.0, 0.0);
}

public DestroySmoke(id)
{
 	DestroyObject(id);
}

// Destroys extinguishing-smoke
public RemoveSmokeFromFire(id)
{
    for(new i; i < 5; i++)
	{
		DestroyObject(Flame[id][Smoke][i]);
		Flame[id][Smoke][i] = -1;
	}
}

public ExtinguishTimer(playerid, id)
{
	if(id < -1 && (Aiming_at_Flame(playerid) == id || Pissing_at_Flame(playerid) == id)) { TogglePlayerBurning(id+MAX_PLAYERS, false); }
	else if(Flame[id][Flame_Exists] && ((Pressing(playerid) & KEY_FIRE && Aiming_at_Flame(playerid) == id) || (Pissing_at_Flame(playerid) == id)))
	{
		new sendername[MAX_PLAYER_NAME+26];
		GetPlayerName(playerid, sendername, sizeof(sendername));
		#if defined MessageToAll
			if(Pissing_at_Flame(playerid) == id)
			{
			    #if defined German
					format(sendername, sizeof(sendername), "* %s hat einen Brand ausgepisst! *", sendername);
			    #else
					format(sendername, sizeof(sendername), "* %s pissed out a fire! *", sendername);
				#endif
			}
			else if(Aiming_at_Flame(playerid) == id)
			{
			    #if defined German
					format(sendername, sizeof(sendername), "* %s hat einen Brand gelöscht! *", sendername);
			    #else
					format(sendername, sizeof(sendername), "* %s extinguished a fire! *", sendername);
				#endif
			}
			SendClientMessageToAll(FireMessageColor, sendername);
		#else
		    if(Pissing_at_Flame(playerid) == id)
			{
			    #if defined German
					SendClientMessage(playerid, FireMessageColor, "* Du hast einen Brand ausgepisst! *");
			    #else
					SendClientMessage(playerid, FireMessageColor, "* You pissed out a fire! *");
				#endif
			}
			else if(Aiming_at_Flame(playerid) == id)
			{
			    #if defined German
					SendClientMessage(playerid, FireMessageColor, "* Du hast einen Brand gelöscht! *");
			    #else
					SendClientMessage(playerid, FireMessageColor, "* You extinguished a fire! *");
				#endif
			}
		#endif
	    DestroyFire(id);
	}
	KillTimer(PlayerFireTimer[playerid][2]);
	PlayerFireTimer[playerid][2] = -1;
}

public TogglePlayerBurning(playerid, burning)
{
	if(burning)
	{
	    SetPlayerAttachedObject(playerid, FIRE_OBJECT_SLOT, 18690, 2, -1, 0, -1.9, 0, 0);
		GetPlayerHealth(playerid, PlayerOnFireHP[playerid]);
		KillTimer(PlayerFireTimer[playerid][0]); KillTimer(PlayerFireTimer[playerid][1]);
		PlayerFireTimer[playerid][0] = SetTimerEx("BurningTimer", 91, 1, "d", playerid);
		PlayerFireTimer[playerid][1] = SetTimerEx("TogglePlayerBurning", 7000, 0, "dd", playerid, 0);
	}
	else
	{
		KillTimer(PlayerFireTimer[playerid][0]);
		RemovePlayerAttachedObject(playerid, FIRE_OBJECT_SLOT);
	}
	SetPVarInt(playerid, "IsOnFire", burning);
	return 1;
}

public BurningTimer(playerid)
{
	if(GetPVarInt(playerid, "IsOnFire"))
	{
	    new Float:hp;
	    GetPlayerHealth(playerid, hp);
	    if(hp < PlayerOnFireHP[playerid])
	    {
	        PlayerOnFireHP[playerid] = hp;
		}
	    PlayerOnFireHP[playerid] -= 1.0;
		SetPlayerHealth(playerid, PlayerOnFireHP[playerid]);
	}
	else { KillTimer(PlayerFireTimer[playerid][0]); KillTimer(PlayerFireTimer[playerid][1]); }
}

//===================== Other Functions ====================

stock GetFireID(Float:x, Float:y, Float:z, &Float:dist)
{
	new id = -1;
	dist = 99999.99;
	for(new i; i < MAX_FIRES; i++)
	{
	    if(GetDistanceBetweenPoints(x,y,z,Flame[i][Flame_pos][0],Flame[i][Flame_pos][1],Flame[i][Flame_pos][2]) < dist)
	    {
	        dist = GetDistanceBetweenPoints(x,y,z,Flame[i][Flame_pos][0],Flame[i][Flame_pos][1],Flame[i][Flame_pos][2]);
	        id = i;
		}
	}
	return id;
}

stock CanPlayerBurn(playerid, val = 0)
{
	if(CallRemoteFunction("CanBurn", "d", playerid) >= 0 && !IsPlayerInWater(playerid) && GetPlayerSkin(playerid) != 277 && GetPlayerSkin(playerid) != 278 && GetPlayerSkin(playerid) != 279 && ((!val && !GetPVarInt(playerid, "IsOnFire")) || (val && GetPVarInt(playerid, "IsOnFire")))) { return 1; }
	return 0;
}

/* //Uncomment or copy to your script.

forward CanBurn(playerid);
public CanBurn(playerid)
{
	if(...)
	{
		return 1;
	}
	return -1; // IMPORTANT!
}

*/

stock IsPlayerInWater(playerid)
{
	new Float:X, Float:Y, Float:Z, an = GetPlayerAnimationIndex(playerid);
	GetPlayerPos(playerid, X, Y, Z);
	if((1544 >= an >= 1538 || an == 1062 || an == 1250) && (Z <= 0 || (Z <= 41.0 && IsPlayerInArea(playerid, -1387, -473, 2025, 2824))) ||
	(1544 >= an >= 1538 || an == 1062 || an == 1250) && (Z <= 2 || (Z <= 39.0 && IsPlayerInArea(playerid, -1387, -473, 2025, 2824))))
	{
	    return 1;
 	}
 	return 0;
}

stock IsPlayerInArea(playerid, Float:MinX, Float:MaxX, Float:MinY, Float:MaxY)
{
	new Float:x, Float:y, Float:z;
	GetPlayerPos(playerid, x, y, z);
	#pragma unused z
    if(x >= MinX && x <= MaxX && y >= MinY && y <= MaxY) { return 1; }
    return 0;
}

stock GetFlameSlot()
{
	for(new i = 0; i < MAX_FIRES; i++)
	{
		if(!Flame[i][Flame_Exists]) { return i; }
	}
	return -1;
}

//===================== "Callbacks" ====================

stock IsAtFlame(playerid)
{
	for(new i; i < MAX_FIRES; i++)
	{
	    if(Flame[i][Flame_Exists])
		{
		    if(!IsPlayerInAnyVehicle(playerid) && (IsPlayerInRangeOfPoint(playerid, BURNING_RADIUS, Flame[i][Flame_pos][0], Flame[i][Flame_pos][1], Flame[i][Flame_pos][2]+Z_DIFFERENCE) ||
												   IsPlayerInRangeOfPoint(playerid, BURNING_RADIUS, Flame[i][Flame_pos][0], Flame[i][Flame_pos][1], Flame[i][Flame_pos][2]+Z_DIFFERENCE-1)))
		    {
				return 1;
			}
		}
	}
	return 0;
}

new AaF_cache[MAX_PLAYERS] = { -1, ... };
new AaF_cacheTime[MAX_PLAYERS];

stock Aiming_at_Flame(playerid)
{
	if(gettime() - AaF_cacheTime[playerid] < 1)
  	{
  	    return AaF_cache[playerid];
 	}
 	AaF_cacheTime[playerid] = gettime();

	new id = -1;
	new Float:dis = 99999.99;
	new Float:dis2;
	new Float:px, Float:py, Float:pz;
	new Float:x, Float:y, Float:z, Float:a;
	GetXYInFrontOfPlayer(playerid, x, y, z, a, 1);
	z -= Z_DIFFERENCE;

	new Float:cx,Float:cy,Float:cz,Float:fx,Float:fy,Float:fz;
	GetPlayerCameraPos(playerid, cx, cy, cz);
	GetPlayerCameraFrontVector(playerid, fx, fy, fz);

	for(new i; i < MAX_PLAYERS; i++)
	{
	    if(IsPlayerConnected(i) && GetPVarInt(i, "IsOnFire") && (IsInWaterCar(playerid) || HasExtinguisher(playerid) || GetPlayerWeapon(playerid) == 41 || Peeing(playerid)) && GetPVarInt(i, "IsOnFire"))
	    {
	        GetPlayerPos(i, px, py, pz);
	        if(!Peeing(playerid))
		 	{
	        	dis2 = DistanceCameraTargetToLocation(cx, cy, cz, px, py, pz, fx, fy, fz);
 			}
 			else
 			{
 			    if(IsPlayerInRangeOfPoint(playerid, ONFOOT_RADIUS, px, py, pz))
				{
	        		dis2 = 0.0;
				}
 			}
	        if(dis2 < dis)
	        {
				dis = dis2;
	    		id = i;
	    		if(Peeing(playerid))
	    		{
	    		    return id;
				}
			}
		}
	}
	if(id != -1) { return id-MAX_PLAYERS; }
	for(new i; i < MAX_FIRES; i++)
	{
		if(Flame[i][Flame_Exists])
		{
		    if(IsInWaterCar(playerid) || HasExtinguisher(playerid) || GetPlayerWeapon(playerid) == 41 || Peeing(playerid))
		    {
		        if(!Peeing(playerid))
				{
					dis2 = DistanceCameraTargetToLocation(cx, cy, cz, Flame[i][Flame_pos][0], Flame[i][Flame_pos][1], Flame[i][Flame_pos][2]+Z_DIFFERENCE, fx, fy, fz);
				}
				else
				{
				    dis2 = GetDistanceBetweenPoints(x,y,z,Flame[i][Flame_pos][0],Flame[i][Flame_pos][1],Flame[i][Flame_pos][2]);
				}
				if((IsPlayerInAnyVehicle(playerid) && dis2 < CAR_RADIUS && dis2 < dis) || (!IsPlayerInAnyVehicle(playerid) && ((dis2 < ONFOOT_RADIUS && dis2 < dis) || (Peeing(playerid) && dis2 < PISSING_DISTANCE && dis2 < dis))))
				{
				    dis = dis2;
				    id = i;
				}
			}
		}
	}
	if(id != -1)
	{
		if
		(
			(
				IsPlayerInAnyVehicle(playerid) && !IsPlayerInRangeOfPoint(playerid, 50, Flame[id][Flame_pos][0], Flame[id][Flame_pos][1], Flame[id][Flame_pos][2])
			)
			||
			(
				!IsPlayerInAnyVehicle(playerid)  && !IsPlayerInRangeOfPoint(playerid, 5, Flame[id][Flame_pos][0], Flame[id][Flame_pos][1], Flame[id][Flame_pos][2])
			)
		)
		{ id = -1; }
	}
	AaF_cache[playerid] = id;
	return id;
}

stock Pissing_at_Flame(playerid)
{
	if(Peeing(playerid))
	{
	    return Aiming_at_Flame(playerid);
	}
	return -1;
}

stock IsInWaterCar(playerid)
{
    if(GetVehicleModel(GetPlayerVehicleID(playerid)) == 407 || GetVehicleModel(GetPlayerVehicleID(playerid)) == 601) { return 1; }
	return 0;
}

stock HasExtinguisher(playerid)
{
    if(GetPlayerWeapon(playerid) == 42 && !IsPlayerInAnyVehicle(playerid)) { return 1; }
	return 0;
}

stock Peeing(playerid)
{
	return GetPlayerSpecialAction(playerid) == SPECIAL_ACTION_PISSING;
}

stock Pressing(playerid)
{
	new keys, updown, leftright;
	GetPlayerKeys(playerid, keys, updown, leftright);
	return keys;
}

//===================== Important Shit ====================

forward MMF_ExtFire(version[15]);
public MMF_ExtFire(version[15])
{
	if(strcmp(VERSION, version, true) && strlen(version))
	{
	    return 2;
	}
	return 1;
}

AntiDeAMX()
{
	new foo[][] =
    {
		"l33t",
        "lol xD"
    };
    #pragma unused foo
}
